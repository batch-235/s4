const express = require('express');
const router = express.Router();

const { users } = require('../src/util');


router.post("/register", (req, res) => {

    if (!req.body.hasOwnProperty("name")) {
        return res.status(400).send({
            error: 'Bad Request - missing NAME property'
        })
    }

    if (!req.body.hasOwnProperty("age")) {
        return res.status(400).send({
            error: 'Bad Request - missing AGE property'
        })
    }

    if (!req.body.hasOwnProperty("username")) {
        return res.status(400).send({
            error: 'Bad Request - missing AGE property'
        })
    }

    if (typeof req.body.age !== "number") {
        return res.status(400).send({
            error: 'Bad Request - AGE must be a number'
        })
    }

})



router.post('/login',(req,res)=>{

    let foundUser = users.find((user) => {

        return user.username === req.body.username && user.password === req.body.password

    });

    if(!req.body.hasOwnProperty('username')){
        return res.status(400).send({
            'error' : 'Bad Request: missing required parameter USERNAME'
        })
    }

    if(!req.body.hasOwnProperty('password')){
        return res.status(400).send({
            'error' : 'Bad Request: missing required parameter PASSWORD'
        })
    }


    if(foundUser){
        return res.status(200).send({
            'success': 'Thank you for logging in.'
        })
    }

    // Stretch Goal
    if(!foundUser) {
        return res.status(403).send({
            'error' : 'Wrong credentials: Wrong email or password'
        })
    }
    

})


module.exports = router;