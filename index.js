const getCircleArea = (r) => {
    if (r <= 0 || typeof r !== "number") return undefined

    return +(Math.PI * (r ** 2)).toFixed(2)
};

module.exports = {
    getCircleArea: getCircleArea
};