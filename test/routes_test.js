const chai = require("chai");
const { assert, expect } = require("chai");

const http = require("chai-http");

chai.use(http);

describe("Test suite register", () => {

    const URL = 'http://localhost:5001';

    it("Test API post register returns 400 if no name", (done) => {
        chai.request(URL)
        .post('/users/register')
        .type('json')
        .send({
            password: "iamjson",
            age: 28
        })
        .end((err, res) => {
            assert.equal(res.status, 400);
            done();
        })
    })


    it ("Test API post register is running", () => {
        chai.request(URL)
        .post('/users/register')
        .type('json')
        .send({
            username: "iamjson",
            name: "Jay White",
            age: 27
        })
        .end((err, res) => {
            assert.notEqual(res.status, 404);
        })
    })


    it ("Test API post register returns 400 if no AGE", (done) => {
        chai.request(URL)
        .post('/users/register')
        .type('json')
        .send({
            username: "iamjson",
            name: "Jay White"
            // age: 27
        })
        .end((err, res) => {
            assert.equal(res.status, 400);
            done();
        })
    })

})



describe("Test user login", () => {
    it('test_api_post_login_returns_400_if_no_password', (done) => {
        chai.request('http://localhost:5001')
        .post('/users/login')
        .type('json')
        .send({
            username: "brBoyd87"
        })
        .end((err, res) => {
            expect(res.status).to.equal(400)
            done();
        })
    })

    it('test_api_post_login_returns_200_if_correct_credentials', (done) => {
        chai.request('http://localhost:5001')
        .post('/users/login')
        .type('json')
        .send({
            username: "brBoyd87",
            password: "87brandon19",
        })
        .end((err, res) => {
            expect(res.status).to.equal(200)
            done();
        })
    })

     // Stretch Goal
    it('test_api_post_login_returns_403_if_wrong_credentials', (done) => {
        chai.request('http://localhost:5001')
        .post('/users/login')
        .type('json')
        .send({
            username: "brBoyd87",
            password: "WRONG_PASSWORD",
        })
        .end((err, res) => {
            expect(res.status).to.equal(403)
            done();
        })
    })
})